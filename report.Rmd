---
title: "Karlovy Vary"
author: "samizdat.cz"
date: "24. června 2015"
output: html_document
---

# Vzdušné
Od karlovarského magistrátu jsme získali měsíční objemy vzdušného, které městu platí lázeňští hosté. Příslušná [vyhláška říká](https://samizdat.cz/data/karlovy-vary/data/vyhlaska.doc), že:

> Předmětem poplatku je přechodné ubytování za úplatu na území města za účelem léčení nebo rekreace.

Festivaloví hosté tedy vzdušné ve výši 15 korun za den neplatí. V červenci, v době konání festivalu, je proto patrný pokles v objemu vybíraného vzdušného. Festivaloví hosté vytlačují ty lázeňské. V následujícím grafu jsou července vyznačené modrým podtiskem.

```{r, echo=FALSE, message=FALSE, fig.width=8, fig.height=4}
library(xlsx)
library(dplyr)
library(ggplot2)
library(scales)
library(RColorBrewer)
vzdusne <- read.xlsx("data/kv_vzdusne.xlsx", 1)
vzdusne <- tbl_df(vzdusne)
vzdusne$mesic <- seq(1:12)
vzdusne$den <- 15
vzdusne$datum <- as.Date(paste0(vzdusne$rok, "-", vzdusne$mesic, "-", vzdusne$den))
ggplot(vzdusne) + 
  aes(datum, vzdusne/1000000) + 
  geom_line(color="red", size=0.5) + 
  ggtitle("Vzdušné vybrané městem Karlovy Vary v letech 2011 až 2014") + 
  ylab("miliony Kč") + 
  xlab("měsíc a rok") + 
  scale_x_date(minor_breaks = date_breaks("month")) +
  annotate("rect", xmin=as.Date("2011-07-01"), xmax=as.Date("2011-07-31"), ymin=1, ymax=2.5, alpha=0.1, fill="blue") +
  annotate("rect", xmin=as.Date("2012-07-01"), xmax=as.Date("2012-07-31"), ymin=1, ymax=2.5, alpha=0.1, fill="blue") +
  annotate("rect", xmin=as.Date("2013-07-01"), xmax=as.Date("2013-07-31"), ymin=1, ymax=2.5, alpha=0.1, fill="blue") +
  annotate("rect", xmin=as.Date("2014-07-01"), xmax=as.Date("2014-07-31"), ymin=1, ymax=2.5, alpha=0.1, fill="blue")
```

# Festivalové statistiky
Zdrojem dat je [web karlovarského festivalu](http://www.kviff.com/cs/historie).

```{r, echo=FALSE, message=FALSE, fig.width=8, fig.height=3}
library(xlsx)
library(dplyr)
library(ggplot2)
library(RColorBrewer)
statistiky <- read.csv("data/festivalove-statistiky.csv")
ggplot(statistiky) +
  aes(rok, vstupenek) +
  geom_bar(stat="identity", fill=brewer.pal(4, name="Set1")[1]) +
  geom_text(aes(label=round(vstupenek/1000, 0)), vjust=1.5, size=3) +
  ggtitle("Počet prodaných vstupenek (v tisících)") +
  ylab("vstupenek")
```

```{r, echo=FALSE, message=FALSE, fig.width=8, fig.height=3}
library(xlsx)
library(dplyr)
library(ggplot2)
library(RColorBrewer)
statistiky <- read.csv("data/festivalove-statistiky.csv")
ggplot(statistiky) +
  aes(rok, akreditovanych) +
  geom_bar(stat="identity", fill=brewer.pal(4, name="Set1")[2]) +
  geom_text(aes(label=akreditovanych), vjust=1.5, size=2.5, color="white") +
  ggtitle("Počet akreditovaných diváků") +
  ylab("akreditovaných")
```

```{r, echo=FALSE, message=FALSE, fig.width=8, fig.height=3}
library(xlsx)
library(dplyr)
library(ggplot2)
library(RColorBrewer)
statistiky <- read.csv("data/festivalove-statistiky.csv")
ggplot(statistiky) +
  aes(rok, filmu) +
  geom_bar(stat="identity", fill=brewer.pal(4, name="Set1")[3]) +
  geom_text(aes(label=filmu), vjust=1.5, size=3) +
  ggtitle("Počet uvedených filmů") +
  ylab("filmů")
```

```{r, echo=FALSE, message=FALSE, fig.width=8, fig.height=3, warning=FALSE}
library(xlsx)
library(dplyr)
library(ggplot2)
library(RColorBrewer)
statistiky <- read.csv("data/festivalove-statistiky.csv")
ggplot(statistiky) +
  aes(rok, projekci) +
  geom_bar(stat="identity", fill=brewer.pal(4, name="Set1")[4]) +
  geom_text(aes(label=projekci), vjust=1.5, size=3) +
  ggtitle("Počet projekcí") +
  ylab("projekcí")
```

